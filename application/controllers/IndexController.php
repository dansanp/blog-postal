<?php

class IndexController extends My_Controller_Action
{



    public function indexAction()
    {

        $model_entrada = new Application_Model_Entrada();


        $paginador=Zend_paginator::factory($model_entrada->select()->where('activo=1'));
        $paginador->setItemCountPerPage(5);
        $paginador->setCurrentPageNumber($this->_request->getParam('page',1));

        $this->view->paginador = $paginador;
    }

    public function usuariosAction()
    {
        $model_entrada = new Application_Model_Usuario();

        $paginador=Zend_paginator::factory($model_entrada->select());
        $paginador->setItemCountPerPage(2);
        $paginador->setCurrentPageNumber($this->_request->getParam('page',1));

        $this->view->paginador = $paginador;
    }

    public function testAction(){
        $model_entrada= new Application_Model_Entrada();
        $entrada = $model_entrada->find(1)->current();

        $tags = $entrada->findManyToManyRowset('Application_Model_Tag','Application_Model_TagsEntradas');

        Zend_Debug::dump($tags->toArray());

        foreach($tags as $tag){
            echo '<h1>'.$tag->name.'</h1>';
            Zend_Debug::dump($tag->findManyToManyRowset('Application_Model_Entrada','Application_Model_TagsEntradas'));
        }
        //Zend_Debug::dump($tags);
        exit();
    }

}

