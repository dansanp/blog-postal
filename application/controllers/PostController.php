<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 25/02/14
 * Time: 16:31
 */
class PostController extends My_Controller_Action{

    public function indexAction(){
        $id=$this->_request->getParam('id',-1);
        if($id<=0){
            $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'default',true);
        }
        $model_entrada=new Application_Model_Entrada();
        $select=$model_entrada->select();
        $select->where("entrada_id = ?",$id);
        $row=$model_entrada->fetchRow($select);
        $tags = $row->findManyToManyRowset('Application_Model_Tag','Application_Model_TagsEntradas');
        $this->view->post=$row;
        $this->view->tags=$tags->toArray();

    }
}