<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 16:56
 */


class Application_Model_Entrada  extends Zend_Db_Table_Abstract{
    protected $_primary = 'entrada_id';
    protected $_name = 'entradas';

    protected $_dependentTables = array('Application_Model_TagsEntradas');

    protected $_referenceMap =array(
        'Editor' =>array(
            'columns' => array('id_editor'),
            'refTableClass' => 'Application_Model_Usuario',
            'refColumns' => array('user_id')
        ),
        'Autor' => array(
            'columns' => array('id_creador'),
            'refTableClass' => 'Application_Model_Usuario',
            'refColumns' => array('user_id')
        )
    );
} 