<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 16:56
 */


class Application_Model_Tag  extends Zend_Db_Table_Abstract{
    protected $_primary = 'tag_id';
    protected $_name = 'tags';

    protected $_dependentTables = array('Application_Model_TagsEntradas');

    public function getTagId($tag){
        $select=$this->select();
        $select->where("name = ?",$tag);
        $rows=$this->fetchAll($select);
        $count=$rows->count();
        if($count==0){
            $this->insert(array('name'=>$tag,'activo'=>1));
            return $this->getAdapter()->lastInsertId();
        }else{

            $row = $rows->getRow(0)->toArray();
            return $row['tag_id'];
        }
    }
} 