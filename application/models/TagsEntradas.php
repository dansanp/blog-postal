<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 16:56
 */


class Application_Model_TagsEntradas  extends Zend_Db_Table_Abstract{
    protected $_primary = array('tags_id','entradas_id');
    protected $_name = 'tags_entradas';

    protected $_referenceMap =array(
        'Entrada' =>array(
            'columns' => array('entradas_id'),
            'refTableClass' => 'Application_Model_Entrada',
            'refColumns' => array('entrada_id')
        ),
        'Tag' =>array(
            'columns' => array('tags_id'),
            'refTableClass' => 'Application_Model_Tag',
            'refColumns' => array('tag_id')
        )
    );

    public function clearTagEntradas($EntradaId){
        $this->delete(array('entradas_id'=>$EntradaId));
    }
} 