<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 16:56
 */


class Application_Model_Usuario  extends Zend_Db_Table_Abstract{
    protected $_primary = 'user_id';
    protected $_name = 'users';

    protected $_dependentTables = array('Application_Model_Entrada');
} 