<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 17:49
 */

class Admin_IndexController extends Admin_Controller_Action{

    public function indexAction(){
        $model_entrada = new Application_Model_Entrada();

        $paginador=Zend_paginator::factory($model_entrada->select());
        $paginador->setItemCountPerPage(5);
        $paginador->setCurrentPageNumber($this->_request->getParam('page',1));

        $this->view->paginador = $paginador;
    }

} 