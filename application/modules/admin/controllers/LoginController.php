<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 17:59
 */

class Admin_LoginController extends Admin_Controller_Action{

    public function init(){
        $auth=Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'admin',true);
        }

    }

    public function indexAction(){

        $form = new Admin_Form_Login();
        if($this->getRequest()->isPost()){
            if(!$form->isValid($this->getRequest()->getParams())){
                $form->populate($this->getRequest()->getParams());
                $this->view->error='Credenciales incorrectas';
            } else{
            //Compruebo autenticacion
                $authAdapter=new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter(),'users','user_name','password');
                $form->populate($this->getRequest()->getParams());
                $authAdapter->setIdentity($form->getValue('usuario'));
                $authAdapter->setCredential($form->getValue('password'));
                $select=$authAdapter->getDbSelect();
                $select->where('activo = 1');
                $authResult = $authAdapter->authenticate();
                $zAuth = Zend_Auth::getInstance();
                $authReuslt = $zAuth->authenticate($authAdapter);
                if($authResult->isValid()){
                    $storage=$zAuth->getStorage();
                    $user=$authAdapter->getResultRowObject(null,'password');
                    $storage->write($user);
                    $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'admin',true);
                }
            }
        }
        $this->view->form=$form;
    }

} 