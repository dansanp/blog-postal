<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 25/02/14
 * Time: 17:25
 */

class Admin_LogoutController extends Admin_Controller_Action{

    public function indexAction(){
        $this->_auth->clearIdentity();
        $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'default',true);
    }
} 