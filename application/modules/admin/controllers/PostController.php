<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 25/02/14
 * Time: 17:58
 */

class Admin_PostController extends Admin_Controller_Action{

    private $_user;

    public function init(){
        parent::init();
        $this->_user=$this->_auth->getStorage()->read();
    }

    public function indexAction(){
        $this->_helper->redirector->gotoRoute(array('action'=>'new','controller'=>'post'),'admin',true);
    }

    public function newAction(){
        $form=new Admin_Form_Post();


        if($this->getRequest()->isPost()){

            if(!$form->isValid($this->getRequest()->getParams())){
                $form->populate($this->getRequest()->getParams());
            } else {

                $form->populate($this->getRequest()->getParams());
                $attribs=$form->getValues();
                $attribs['id_creador']=$this->_user->user_id;
                $attribs['id_editor']=$this->_user->user_id;
                $attribs['fecha_creacion']=new Zend_Db_Expr('CURRENT_TIMESTAMP');
                $attribs['fecha_edicion']=new Zend_Db_Expr('CURRENT_TIMESTAMP');
                $attribs['activo']=1;
                $arr_tag=explode(',',$attribs['tags']);
                unset ($attribs['tags']);

                $m_entrada=new Application_Model_Entrada();
                $m_entrada->insert($attribs);
                $idEntrada=$m_entrada->getAdapter()->lastInsertId();



                $m_tag=new Application_Model_Tag();
                $m_tagEntrada=new Application_Model_TagsEntradas();
                //Obtenemos los Id's de los tags y los introducimos en la tabla
                foreach($arr_tag as $elem){
                    $tag_id=$m_tag->getTagId($elem);
                    $m_tagEntrada->insert(array('tags_id'=>$tag_id, 'entradas_id'=>$idEntrada));
                }

                $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'admin',true);

            }

        }
        $this->view->form=$form;

    }

    public function editAction(){
        $id=$this->getRequest()->getParam('id',0);
        if($id>0){
            $formulario = new Admin_Form_Post();
            $model=new Application_Model_Entrada();
            if ($this->getRequest()->isPost()){
                if(!$formulario->isValid($this->getRequest()->getParams())){
                    $formulario->populate($this->getRequest()->getParams());
                }else{
                    $formulario->populate($this->getRequest()->getParams());
                    $attribs=$formulario->getValues();
                    $attribs['id_editor']=$this->_user->user_id;
                    $attribs['fecha_edicion']=new Zend_Db_Expr('CURRENT_TIMESTAMP');
                    $arr_tag=explode(',',$attribs['tags']);
                    $m_entrada=new Application_Model_Entrada();
                    unset ($attribs['tags']);
                    $m_entrada->update($attribs,"entrada_id=$id");
                    $m_tagEntrada=new Application_Model_TagsEntradas();
                    $m_tag=new Application_Model_Tag();
                    //Borramos los tags de esta entrada
                    $m_tagEntrada->clearTagEntradas($id);
                    foreach($arr_tag as $elem){
                        $tag_id=$m_tag->getTagId($elem);
                        $m_tagEntrada->insert(array('tags_id'=>$tag_id, 'entradas_id'=>$id));
                    }

                    $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'admin',true);
                }
            }else{
                $select=$model->select();
                $select->where("entrada_id = ?",$id);
                $row=$model->fetchRow($select);
                $tags = $row->findManyToManyRowset('Application_Model_Tag','Application_Model_TagsEntradas');
                $attribs=$row->toArray();
                $cont=0;
                $strTag='';

                foreach($tags as $tag){
                    if($cont>0){
                        $strTag.=',';
                    }
                    $strTag.=$tag['name'];
                    $cont++;
                }
                $attribs['tags']=$strTag;
                $formulario->populate($attribs);
                $formulario->setEditMode();
            }
            $this->view->form=$formulario;

        }

    }

    public function deleteAction(){
        $id=$this->getRequest()->getParam('id',0);
        if($id>0){
            $model=new Application_Model_Entrada();
            $model->update(array('activo'=>0),"entrada_id=$id");
        }
        $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'index'),'admin',true);


    }
} 