<?php

class Admin_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->addElement('text','usuario');
        $this->getElement('usuario')
            ->setLabel('USUARIO')
            ->setRequired(true)
            ->addValidator(new Zend_Validate_Alnum());

        $this->addElement('password','password');
        $this->getElement('password')
            ->setLabel('PASSWORD')
            ->setRequired(true);

        $this->addElement('submit','submit');
        $this->getElement('submit')
            ->setLabel('ENVIAR');
    }


}

