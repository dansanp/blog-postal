<?php

class Admin_Form_Post extends Zend_Form{


    public function init(){
        $this->addElement('text','titulo');
        $this->getElement('titulo')
            ->setLabel('titulo')
            ->addValidator(new Zend_Validate_StringLength(array('min'=>1,'max'=>255)))
            ->setRequired(true);

        $this->addElement('text','entradilla');
        $this->getElement('entradilla')
            ->setLabel('Entradilla')
            ->setRequired(true);

        $this->addElement(new My_Form_Element_CKEditor('contenido', array('required'=>true, 'label'=>'Contenido')));

        $this->addElement('text','tags');
        $this->getElement('tags')
            ->setLabel('Tags del mensaje (Separadas por comas');


        $this->addElement('submit','submit');
        $this->getElement('submit')
            ->setLabel('Enviar');
    }

    public function setEditMode(){
        $this->getElement('submit')
            ->setLabel('Editar');
    }




}