<?php


class My_View_Helper_FormatearFecha extends Zend_View_Helper_Abstract{

    public function formatearFecha($fechaSql){


        $date=new DateTime($fechaSql);
        return $date->format('d/m/Y H:i:s');
    }
}