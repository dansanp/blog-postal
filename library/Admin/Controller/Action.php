<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 21/02/14
 * Time: 17:54
 */

class Admin_Controller_Action extends Zend_Controller_Action{
    protected $_auth;
    public function init(){
        $this->_auth=Zend_Auth::getInstance();
        if(!$this->_auth->hasIdentity())
        {
            $this->_helper->redirector->gotoRoute(array('action'=>'index','controller'=>'login'),'admin',true);
        }
        $this->_helper->layout->setLayout('admin');
}

} 